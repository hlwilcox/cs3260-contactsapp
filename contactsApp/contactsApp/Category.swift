//
//  Category.swift
//  contactsApp
//
//  Created by Heather Wilcox on 3/22/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit
import RealmSwift

class Category: Object {
    @objc dynamic var name = ""
    @objc dynamic var age = 0
    //@objc dynamic var firstName = ""
}
