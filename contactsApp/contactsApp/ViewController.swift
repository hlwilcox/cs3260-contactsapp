//
//  ViewController.swift
//  contactsApp
//
//  Created by Heather Wilcox on 3/22/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let realm = try! Realm()
        let categ = Category()
        
        categ.name = "Name"
        categ.age = 25
        
        try! realm.write() {
            realm.add(categ)
        }
        
        let categories = realm.objects(Category.self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

